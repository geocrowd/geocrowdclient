package com.example.privategeocrowdclient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class Utility {

	protected static void Display_Worker(String[] s, MapView mv,
			Drawable marker, Context ct) {

		int markerWidth = marker.getIntrinsicWidth();
		int markerHeight = marker.getIntrinsicHeight();
		marker.setBounds(0, markerHeight, markerWidth, 0);
		Overlaying_Item Overlay = new Overlaying_Item(marker, ct);
		for (int i = 0; i < s.length - 1; i = i + 2) {
			GeoPoint t = new GeoPoint((int) (Double.parseDouble(s[i]) * 1E6),
					(int) (Double.parseDouble(s[i + 1]) * 1E6));
			Overlay.addItem(t, "taxi", "abc");
		}
		if (mv.getOverlays().size() > 1) {
			mv.getOverlays().remove(1);
			mv.getOverlays().add(1, Overlay);
		} else {
			mv.getOverlays().add(Overlay);
		}
	}
	
	protected static void Display_Rect(GeoPoint tl, GeoPoint br, MapView mv, Context ct) {

		
		Overlaying_Rect Overlay = new Overlaying_Rect(tl, br, ct);
		
		mv.getOverlays().add(Overlay);
	}

	protected static void Display_Worker(ArrayList<Worker> w, MapView mv,
			Drawable marker1, Drawable marker2, Context ct) {

		int markerWidth = marker1.getIntrinsicWidth();
		int markerHeight = marker1.getIntrinsicHeight();
		marker1.setBounds(0, markerHeight, markerWidth, 0);
		marker2.setBounds(0, markerHeight, markerWidth, 0);
		Overlaying_Item Overlay_notified = new Overlaying_Item(marker1, ct);
		Overlaying_Item Overlay_accepted = new Overlaying_Item(marker2, ct);

		for (int i = 0; i < w.size(); i++) {
			if (w.get(i) != null) {
				if (w.get(i).isNotified == false) {
					Overlay_notified.addItem(w.get(i).location, w.get(i).ID,
							"Accepting Rate: " + w.get(i).acc_rate);
					Log.d("OVERLAY", "ADDED");
				} else
					Overlay_accepted.addItem(w.get(i).location, w.get(i).ID,
							"Accepting Rate: " + w.get(i).acc_rate);
			}
			else 
				Log.d("OVERLAY_FAILED", "WORKER LIST EMPTY");
		}
		if (mv.getOverlays().size() > 1) {
			if (mv.getOverlays().size() > 2) {
				mv.getOverlays().remove(2);
				mv.getOverlays().add(2, Overlay_accepted);
			}
			mv.getOverlays().remove(1);
			mv.getOverlays().add(1, Overlay_notified);
		} else {
			mv.getOverlays().add(Overlay_notified);
			mv.getOverlays().add(Overlay_accepted);
		}
	}

	protected static void Display_Me_icon(GeoPoint p, MapView mapview,
			Drawable marker, MapController mc, Context t) {

		int markerWidth = marker.getIntrinsicWidth();
		int markerHeight = marker.getIntrinsicHeight();
		marker.setBounds(0, markerHeight, markerWidth, 0);
		Overlaying_Item Overlay = new Overlaying_Item(marker, t);
		Overlay.addItem(p, "taxi", "abc");
		mc.animateTo(p);

		if (mapview.getOverlays().size() > 0) {
			mapview.getOverlays().remove(0);
			mapview.getOverlays().add(0, Overlay);

		} else
			mapview.getOverlays().add(Overlay);

	
		
	}

}