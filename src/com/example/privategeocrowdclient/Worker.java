package com.example.privategeocrowdclient;

import com.google.android.maps.GeoPoint;
public class Worker {
	protected  GeoPoint location;
	protected double acc_rate;
	protected boolean isNotified;
	protected double travel_distance;
	protected String ID;
	
	public Worker (GeoPoint l, String Name){
		location = l;
		ID = Name;
	}
	public Worker (GeoPoint l, String Name, double a_r, double t_d, boolean notified){
		location = l;
		acc_rate = a_r;
		travel_distance = t_d;
		isNotified = notified;
		ID = Name;
	}

}
