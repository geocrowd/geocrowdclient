package com.example.privategeocrowdclient;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class Overlaying_Item extends ItemizedOverlay<OverlayItem> {

	private ArrayList<OverlayItem> overlayItemList = new ArrayList<OverlayItem>();


	Context t;
	public Overlaying_Item(Drawable marker, Context c) {
		super(boundCenterBottom(marker));
		// TODO Auto-generated constructor stub

		t = c;
		populate();
	}

	public void addItem(GeoPoint p, String title, String snippet) {
		OverlayItem newItem = new OverlayItem(p, title, snippet);
		overlayItemList.add(newItem);
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		// TODO Auto-generated method stub
		return overlayItemList.get(i);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return overlayItemList.size();
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		// TODO Auto-generated method stub
		super.draw(canvas, mapView, shadow);
		// boundCenterBottom(marker);
		
	}
	

	@Override
    protected boolean onTap(int index)
    {
        OverlayItem item = overlayItemList.get(index);

      
        //Do stuff here when you tap, i.e. :
        AlertDialog.Builder dialog = new AlertDialog.Builder(t);
        dialog.setTitle(item.getTitle());
        dialog.setMessage(item.getSnippet());
        dialog.show();

        //return true to indicate we've taken care of it
        return true;
    }

}