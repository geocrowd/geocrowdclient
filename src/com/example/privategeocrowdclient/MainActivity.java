package com.example.privategeocrowdclient;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class MainActivity extends MapActivity {
	MapView mapview;
	MapController mc;
	LocationManager lm;
	LocationListener locationListener;
	Drawable me, worker1, worker2;
	ProgressDialog myProgressDialog = null;
	ToggleButton b;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mapview = (MapView) findViewById(R.id.mapview);
		mapview.setBuiltInZoomControls(true);
		me = getResources().getDrawable(R.drawable.me);
		worker1 = getResources().getDrawable(R.drawable.worker1);
		worker2 = getResources().getDrawable(R.drawable.worker2);

		mc = mapview.getController();
		mc.setZoom(14);
		Utility.Display_Me_icon(Static.current, mapview, me, mc,
				MainActivity.this);
		try {

			locationListener = new MyLocationListener();
			lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

			Location loc = lm.getLastKnownLocation("gps");
			if (loc != null) {
				Static.current = new GeoPoint((int) (loc.getLatitude() * 1E6),
						(int) (loc.getLongitude() * 1E6));
			}

			lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0 * 1000,
					50, locationListener);
		} catch (Exception e) {
			Log.d("error", e.toString());
		}

		b = (ToggleButton) findViewById(R.id.toggleButton1);
		b.setText("Show Worker");
		try {
			b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					if (isChecked) {
						//Utility.Display_Worker(Static.empty, mapview, worker1,
							//	MainActivity.this);
						new GetWorkerCoordinate().execute();
						b.setTextOn("Hide Worker");

					} else {
						b.setTextOff("Show Worker");
				//		Utility.Display_Worker(Static.empty, mapview, worker1,
					//			MainActivity.this);
						mapview.invalidate();
					}
				}
			});
		} catch (Exception e) {
			Log.d("errorMapButton", e.toString());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private class MyLocationListener implements LocationListener {
		public void onLocationChanged(Location loc) {
			if (loc != null) {
				Static.current = new GeoPoint((int) (loc.getLatitude() * 1E6),
						(int) (loc.getLongitude() * 1E6));
				try {
					// mapview.getOverlays().remove(0);
				} catch (Exception e) {
					Log.d("PutMeIcon", e.toString());
				}
				Utility.Display_Me_icon(Static.current, mapview, me, mc,
						MainActivity.this);
			} else {
				Log.d("error", "bug in MyLocationListener");
			}

		}

		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		
			
		}

	}
	
	private class GetWorkerCoordinate extends AsyncTask<Void, Void, Void> {

		protected String[] worker_coordinate;
		protected ArrayList<Worker> wk = new ArrayList<Worker>();
		Worker w;

		protected void onPreExecute() {
			myProgressDialog = ProgressDialog.show(MainActivity.this,
					"Please wait...", "Retreiving Geo_Data from Server...",
					true);
			myProgressDialog.setCancelable(true);

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Thread timer = new Thread() {
				public void run() {
					try {

						for (int i = 0; i < Static.sample_worker_location.length - 1; i = i + 2) {
							GeoPoint t = new GeoPoint(
									(int) (Double
											.parseDouble(Static.sample_worker_location[i]) * 1E6),
									(int) (Double
											.parseDouble(Static.sample_worker_location[i + 1]) * 1E6));

							if (i == 0) {
								w = new Worker(t, "This is worker No. " + i,
										0.1 + (i / 5), 12, true);
							} else
								w = new Worker(t, "This is worker No. " + i,
										0.1 + (i / 5), 12, false);
							
							wk.add(w);
							Log.d("TEST", "Creat and Add Worker");

						}
						sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						// Intent openMain = new
						// Intent("com.example.Client.FirstMenu");
						// startActivity(openMain);

						// worker_coordinate = Static.sample_worker_location;

					}
				}

			};
			timer.start();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.d("POST EXECUTE", "PREPARING TO OVERLAY");
			Overlaying_Rect Overlay = new Overlaying_Rect(wk.get(0).location, wk.get(1).location, MainActivity.this);
			
			mapview.getOverlays().add(Overlay);
		
			mapview.invalidate();
			myProgressDialog.dismiss();
		}

	}

}
