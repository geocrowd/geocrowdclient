package com.example.privategeocrowdclient;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;


import com.google.android.maps.GeoPoint;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;

public class Overlaying_Rect extends Overlay {

	private ArrayList<OverlayItem> overlayRect = new ArrayList<OverlayItem>();

	Context t;
	GeoPoint top_left, bottom_right;

	public Overlaying_Rect(GeoPoint t_l, GeoPoint b_r, Context c) {
		top_left = t_l;
		bottom_right = b_r;
		t = c;

	}

	public void addItem(GeoPoint p, String title, String snippet) {
		OverlayItem newItem = new OverlayItem(p, title, snippet);
		overlayRect.add(newItem);

	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		// TODO Auto-generated method stub
		super.draw(canvas, mapView, shadow);
		// boundCenterBottom(marker);
		Projection projection = mapView.getProjection();
		Point point_top_left = new Point();
		Point point_bottom_right = new Point();
		projection.toPixels(top_left, point_top_left);
		projection.toPixels(bottom_right, point_bottom_right);

		// the circle to mark the spot
		Paint rect = new Paint();
		rect.setColor(Color.parseColor ("#88ff0000"));
		//rect.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
	
		canvas.drawRect(point_top_left.x, point_top_left.y,
				point_bottom_right.x, point_bottom_right.y, rect);

	}

	protected boolean onTap(int index) {
		OverlayItem item = overlayRect.get(index);

		// Do stuff here when you tap, i.e. :
		AlertDialog.Builder dialog = new AlertDialog.Builder(t);
		dialog.setTitle("alo");
		dialog.setMessage("DISPLAY");
		dialog.show();

		// return true to indicate we've taken care of it
		return true;
	}

}